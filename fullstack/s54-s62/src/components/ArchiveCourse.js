import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ArchiveCourse({ course, fetchData, isActive }) {

	const [ courseId ] = useState(course);

	// console.log(courseId); // check

	const archiveToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Archived!',
					icon: 'success',
					text: 'Course archived successfully'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	//For checking of url
	// const archiveToggle = () => {
	// 	const url = `${process.env.REACT_APP_API_URL}/courses/${courseId}/archive/`;
  	// 	console.log(url);
	// }

	const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Activated!',
					icon: 'success',
					text: 'Course activated successfully'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}




	return(
		<>
			{(isActive === true) ? 
				<Button variant="danger" size="sm" onClick={archiveToggle}>Archive</Button>
			:
				<Button variant="success" size="sm" onClick={activateToggle}>Activate</Button>
			}

		</>
	)
}